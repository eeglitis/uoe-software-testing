import static org.junit.Assert.*;
import org.junit.Test;

public class Task4_s1353184 {

	@Test
	public void test1() {
		assertEquals("A\"Y\"A\"X\"A", StringUtils2.replaceString("A\"X\"A\"X\"A", "X", "Y", '\"', true));
	}
	
	@Test
	public void test2() {
		assertEquals("A\"Y\"A\"X\"A\"A\"A", StringUtils2.replaceString("A\"X\"A\"X\"A\"A\"A", "X", "Y", '\"', true));
	}

	@Test
	public void test3() {
		assertEquals("A\"Y\"A\"A\"A\"X\"A", StringUtils2.replaceString("A\"X\"A\"A\"A\"X\"A", "X", "Y", '\"', true));
	}

	@Test
	public void test4() {
		assertEquals("A\"A\"A\"Y\"A\"X\"A", StringUtils2.replaceString("A\"A\"A\"X\"A\"X\"A", "X", "Y", '\"', true));
	}
	
	@Test
	public void test5() {
		assertEquals("Y\"A\"X", StringUtils2.replaceString("X\"A\"X", "X", "Y", '\"', false));
	}
	
	@Test
	public void test6() {
		assertEquals("Y\"A\"X\"A\"A", StringUtils2.replaceString("X\"A\"X\"A\"A", "X", "Y", '\"', false));
	}
	
	@Test
	public void test7() {
		assertEquals("Y\"A\"A\"A\"X", StringUtils2.replaceString("X\"A\"A\"A\"X", "X", "Y", '\"', false));
	}
	
	@Test
	public void test8() {
		assertEquals("A\"A\"Y\"A\"X", StringUtils2.replaceString("A\"A\"X\"A\"X", "X", "Y", '\"', false));
	}
}
