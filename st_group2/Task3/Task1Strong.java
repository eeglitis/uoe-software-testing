import static org.junit.Assert.*;

import org.junit.Test;

public class Task1Strong {

	@Test // 1 AR in IT, 1 P in AR, 1 AR with P, R null
	public void test1() {
		assertEquals("a\"\"a", StringUtils.replaceString("a\"XY\"a", "XY", null, '\"', true));
	}
	
	@Test // Multiple AR in IT, 1 P in AR, 1 AR with P, R null 
	public void test2() {
		assertEquals("a\"\"a\"a\"a", StringUtils.replaceString("a\"XY\"a\"a\"a", "XY", null, '\"', true));
	}
	
	@Test // 1 AR in IT, multiple P in AR, 1 AR with P, R null 
	public void test3() {
		assertEquals("a\"\"a", StringUtils.replaceString("a\"XYXY\"a", "XY", null, '\"', true));
	}
	
	@Test // Multiple AR in IT, multiple P in AR, 1 AR with P, R null 
	public void test4() {
		assertEquals("a\"\"a\"a\"a", StringUtils.replaceString("a\"XYXY\"a\"a\"a", "XY", null, '\"', true));
	}
	
	@Test // Multiple AR in IT, 1 P in AR, multiple AR with P, R null
	public void test5() {
		assertEquals("a\"\"a\"\"a", StringUtils.replaceString("a\"XY\"a\"XY\"a", "XY", null, '\"', true));
	}
	
	@Test // Multiple AR in IT, multiple P in AR, multiple AR with P, R null 
	public void test6() {
		assertEquals("a\"\"a\"\"a", StringUtils.replaceString("a\"XYXY\"a\"XYXY\"a", "XY", null, '\"', true));
	}
	
	@Test // 1 AR in IT, 1 P in AR, 1 AR with P, P longer than R, R not null 
	public void test7() {
		assertEquals("a\"JK\"a", StringUtils.replaceString("a\"XYZ\"a", "XYZ", "JK", '\"', true));
	}
	
	@Test // Multiple AR in IT, 1 P in AR, 1 AR with P, P longer than R, R not null
	public void test8() {
		assertEquals("a\"JK\"a\"a\"a", StringUtils.replaceString("a\"XYZ\"a\"a\"a", "XYZ", "JK", '\"', true));
	}
	
	@Test // 1 AR in IT, multiple P in AR, 1 AR with P, P longer than R, R not null
	public void test9() {
		assertEquals("a\"JKJK\"a", StringUtils.replaceString("a\"XYZXYZ\"a", "XYZ", "JK", '\"', true));
	}
	
	@Test // Multiple AR in IT, multiple P in AR, 1 AR with P, P longer than R, R not null
	public void test10() {
		assertEquals("a\"JKJK\"a\"a\"a", StringUtils.replaceString("a\"XYZXYZ\"a\"a\"a", "XYZ", "JK", '\"', true));
	}
	
	@Test // Multiple AR in IT, 1 P in AR, multiple AR with P, P longer than R, R not null
	public void test11() {
		assertEquals("a\"JK\"a\"JK\"a", StringUtils.replaceString("a\"XYZ\"a\"XYZ\"a", "XYZ", "JK", '\"', true));
	}
	
	@Test // Multiple AR in IT, multiple P in AR, multiple AR with P, P longer than R, R not null
	public void test12() {
		assertEquals("a\"JKJK\"a\"JKJK\"a", StringUtils.replaceString("a\"XYZXYZ\"a\"XYZXYZ\"a", "XYZ", "JK", '\"', true));
	}
	
	@Test // 1 AR in IT, 1 P in AR, 1 AR with P, P same length as R
	public void test13() {
		assertEquals("a\"JK\"a", StringUtils.replaceString("a\"XY\"a", "XY", "JK", '\"', true));
	}
	
	@Test // Multiple AR in IT, 1 P in AR, 1 AR with P, P same length as R
	public void test14() {
		assertEquals("a\"JK\"a\"a\"a", StringUtils.replaceString("a\"XY\"a\"a\"a", "XY", "JK", '\"', true));
	}
	
	@Test // 1 AR in IT, multiple P in AR, 1 AR with P, P same length as R
	public void test15() {
		assertEquals("a\"JKJK\"a", StringUtils.replaceString("a\"XYXY\"a", "XY", "JK", '\"', true));
	}
	
	@Test // Multiple AR in IT, multiple P in AR, 1 AR with P, P same length as R
	public void test16() {
		assertEquals("a\"JKJK\"a\"a\"a", StringUtils.replaceString("a\"XYXY\"a\"a\"a", "XY", "JK", '\"', true));
	}
	
	@Test // Multiple AR in IT, 1 P in AR, multiple AR with P, P same length as R
	public void test17() {
		assertEquals("a\"JK\"a\"JK\"a", StringUtils.replaceString("a\"XY\"a\"XY\"a", "XY", "JK", '\"', true));
	}
	
	@Test // Multiple AR in IT, multiple P in AR, multiple AR with P, P same length as R
	public void test18() {
		assertEquals("a\"JKJK\"a\"JKJK\"a", StringUtils.replaceString("a\"XYXY\"a\"XYXY\"a", "XY", "JK", '\"', true));
	}
	
	@Test // 1 AR in IT, 1 P in AR, 1 AR with P, P shorter than R
	public void test19() {
		assertEquals("a\"JK\"a", StringUtils.replaceString("a\"X\"a", "X", "JK", '\"', true));
	}
	
	@Test // Multiple AR in IT, 1 P in AR, 1 AR with P, P shorter than R
	public void test20() {
		assertEquals("a\"JK\"a\"a\"a", StringUtils.replaceString("a\"X\"a\"a\"a", "X", "JK", '\"', true));
	}
	
	@Test // 1 AR in IT, multiple P in AR, 1 AR with P, P shorter than R
	public void test21() {
		assertEquals("a\"JKJK\"a", StringUtils.replaceString("a\"XX\"a", "X", "JK", '\"', true));
	}
	
	@Test // Multiple AR in IT, multiple P in AR, 1 AR with P, P shorter than R
	public void test22() {
		assertEquals("a\"JKJK\"a\"a\"a", StringUtils.replaceString("a\"XX\"a\"a\"a", "X", "JK", '\"', true));
	}
	
	@Test // Multiple AR in IT, 1 P in AR, multiple AR with P, P shorter than R
	public void test23() {
		assertEquals("a\"JK\"a\"JK\"a", StringUtils.replaceString("a\"X\"a\"X\"a", "X", "JK", '\"', true));
	}
	
	@Test // Multiple AR in IT, multiple P in AR, multiple AR with P, P shorter than R
	public void test24() {
		assertEquals("a\"JKJK\"a\"JKJK\"a", StringUtils.replaceString("a\"XX\"a\"XX\"a", "X", "JK", '\"', true));
	}

	@Test // used to kill M1
	public void test25() {
		assertEquals("\"JK\"XX\"a\"", StringUtils.replaceString("\"X\"XX\"a\"", "X", "JK", '\"', true));
	}
	
	@Test // used to kill M2
	public void test26() {
		assertEquals("ZZaaa", StringUtils.replaceString("XYXYaaa", "XY", "Z", '\"', false));
	}
	
	@Test // used to kill M5, M7
	public void test27() {
		assertEquals("\"JK\"XX\"a\"", StringUtils.replaceString("\"X\"XX\"a\"", "X\"", "JK", '\"', true));
	}
		
}
