import static org.junit.Assert.*;
import org.junit.Test;

public class Task2 {
			
	@Test // 1 AR in IT, 1 P in AR, 1 AR with P, R null
	public void test1() {
		assertEquals("a\"\"a", StringUtils.replaceString("a\"XY\"a", "XY", null, '\"', true));
	}
	
	@Test // Multiple AR in IT, 1 P in AR, 1 AR with P, R null 
	public void test2() {
		assertEquals("a\"\"a\"a\"a", StringUtils.replaceString("a\"XY\"a\"a\"a", "XY", null, '\"', true));
	}
	
	@Test // 1 AR in IT, multiple P in AR, 1 AR with P, R null 
	public void test3() {
		assertEquals("a\"\"a", StringUtils.replaceString("a\"XYXY\"a", "XY", null, '\"', true));
	}
	
	@Test // Multiple AR in IT, multiple P in AR, 1 AR with P, R null 
	public void test4() {
		assertEquals("a\"\"a\"a\"a", StringUtils.replaceString("a\"XYXY\"a\"a\"a", "XY", null, '\"', true));
	}
	
	@Test // Multiple AR in IT, 1 P in AR, multiple AR with P, R null
	public void test5() {
		assertEquals("a\"\"a\"\"a", StringUtils.replaceString("a\"XY\"a\"XY\"a", "XY", null, '\"', true));
	}
	
	@Test // Multiple AR in IT, multiple P in AR, multiple AR with P, R null 
	public void test6() {
		assertEquals("a\"\"a\"\"a", StringUtils.replaceString("a\"XYXY\"a\"XYXY\"a", "XY", null, '\"', true));
	}
	
	@Test // 1 AR in IT, 1 P in AR, 1 AR with P, P longer than R, R not null 
	public void test7() {
		assertEquals("a\"JK\"a", StringUtils.replaceString("a\"XYZ\"a", "XYZ", "JK", '\"', true));
	}
	
	@Test // Multiple AR in IT, 1 P in AR, 1 AR with P, P longer than R, R not null
	public void test8() {
		assertEquals("a\"JK\"a\"a\"a", StringUtils.replaceString("a\"XYZ\"a\"a\"a", "XYZ", "JK", '\"', true));
	}
	
	@Test // 1 AR in IT, multiple P in AR, 1 AR with P, P longer than R, R not null
	public void test9() {
		assertEquals("a\"JKJK\"a", StringUtils.replaceString("a\"XYZXYZ\"a", "XYZ", "JK", '\"', true));
	}
	
	@Test // Multiple AR in IT, multiple P in AR, 1 AR with P, P longer than R, R not null
	public void test10() {
		assertEquals("a\"JKJK\"a\"a\"a", StringUtils.replaceString("a\"XYZXYZ\"a\"a\"a", "XYZ", "JK", '\"', true));
	}
	
	@Test // Multiple AR in IT, 1 P in AR, multiple AR with P, P longer than R, R not null
	public void test11() {
		assertEquals("a\"JK\"a\"JK\"a", StringUtils.replaceString("a\"XYZ\"a\"XYZ\"a", "XYZ", "JK", '\"', true));
	}
	
	@Test // Multiple AR in IT, multiple P in AR, multiple AR with P, P longer than R, R not null
	public void test12() {
		assertEquals("a\"JKJK\"a\"JKJK\"a", StringUtils.replaceString("a\"XYZXYZ\"a\"XYZXYZ\"a", "XYZ", "JK", '\"', true));
	}
	
	@Test // 1 AR in IT, 1 P in AR, 1 AR with P, P same length as R
	public void test13() {
		assertEquals("a\"JK\"a", StringUtils.replaceString("a\"XY\"a", "XY", "JK", '\"', true));
	}
	
	@Test // Multiple AR in IT, 1 P in AR, 1 AR with P, P same length as R
	public void test14() {
		assertEquals("a\"JK\"a\"a\"a", StringUtils.replaceString("a\"XY\"a\"a\"a", "XY", "JK", '\"', true));
	}
	
	@Test // 1 AR in IT, multiple P in AR, 1 AR with P, P same length as R
	public void test15() {
		assertEquals("a\"JKJK\"a", StringUtils.replaceString("a\"XYXY\"a", "XY", "JK", '\"', true));
	}
	
	@Test // Multiple AR in IT, multiple P in AR, 1 AR with P, P same length as R
	public void test16() {
		assertEquals("a\"JKJK\"a\"a\"a", StringUtils.replaceString("a\"XYXY\"a\"a\"a", "XY", "JK", '\"', true));
	}
	
	@Test // Multiple AR in IT, 1 P in AR, multiple AR with P, P same length as R
	public void test17() {
		assertEquals("a\"JK\"a\"JK\"a", StringUtils.replaceString("a\"XY\"a\"XY\"a", "XY", "JK", '\"', true));
	}
	
	@Test // Multiple AR in IT, multiple P in AR, multiple AR with P, P same length as R
	public void test18() {
		assertEquals("a\"JKJK\"a\"JKJK\"a", StringUtils.replaceString("a\"XYXY\"a\"XYXY\"a", "XY", "JK", '\"', true));
	}
	
	@Test // 1 AR in IT, 1 P in AR, 1 AR with P, P shorter than R
	public void test19() {
		assertEquals("a\"JK\"a", StringUtils.replaceString("a\"X\"a", "X", "JK", '\"', true));
	}
	
	@Test // Multiple AR in IT, 1 P in AR, 1 AR with P, P shorter than R
	public void test20() {
		assertEquals("a\"JK\"a\"a\"a", StringUtils.replaceString("a\"X\"a\"a\"a", "X", "JK", '\"', true));
	}
	
	@Test // 1 AR in IT, multiple P in AR, 1 AR with P, P shorter than R
	public void test21() {
		assertEquals("a\"JKJK\"a", StringUtils.replaceString("a\"XX\"a", "X", "JK", '\"', true));
	}
	
	@Test // Multiple AR in IT, multiple P in AR, 1 AR with P, P shorter than R
	public void test22() {
		assertEquals("a\"JKJK\"a\"a\"a", StringUtils.replaceString("a\"XX\"a\"a\"a", "X", "JK", '\"', true));
	}
	
	@Test // Multiple AR in IT, 1 P in AR, multiple AR with P, P shorter than R
	public void test23() {
		assertEquals("a\"JK\"a\"JK\"a", StringUtils.replaceString("a\"X\"a\"X\"a", "X", "JK", '\"', true));
	}
	
	@Test // Multiple AR in IT, multiple P in AR, multiple AR with P, P shorter than R
	public void test24() {
		assertEquals("a\"JKJK\"a\"JKJK\"a", StringUtils.replaceString("a\"XX\"a\"XX\"a", "X", "JK", '\"', true));
	}

	@Test // P.length > IT.length
	public void test25() {
		assertEquals("AA", StringUtils.replaceString("AA", "AAA", "BB", '\"', true));
	}
	
	@Test // P.length = IT.length; P!=IT
	public void test26() {
		assertEquals("cc", StringUtils.replaceString("cc", "AA", "BB", '\"', true));
	}
	
	@Test // P=IT; IF=false
	public void test27() {
		assertEquals("BB", StringUtils.replaceString("AA", "AA", "BB", '\"', false));
	}
	
	@Test // No AR, IF=true
	public void test28() {
		assertEquals("ccAA", StringUtils.replaceString("ccAA", "AA", "BB", '\"', true));
	}
	
	@Test // No AR, IF=false
	public void test29() {
		assertEquals("ccBB", StringUtils.replaceString("ccAA", "AA", "BB", '\"', false));
	}
	
	@Test // IF=false; P between AR; IT[0] = IT[IT.length-1] = D; R = NULL; EC in P; D in P
	public void test30() {
		assertEquals("\"cc\"\"c\"ccc\"cc\"", StringUtils.replaceString("\"cc\"AA\"c\"ccc\"cc\"", "A\"eA", null, '\"', false));
	}

	@Test // IF=false; P not in CR
	public void test31() {
		assertEquals("BB\"cc\"c\"ccc\"cc\"c", StringUtils.replaceString("AA\"cc\"c\"ccc\"cc\"c", "AA", "BB", '\"', false));
	}

	@Test // IF=false; P in AR
	public void test32() {
		assertEquals("cc\"cc\"c\"BBc\"cc\"c", StringUtils.replaceString("cc\"cc\"c\"AAc\"cc\"c", "AA", "BB", '\"', false));
	}

	@Test // IF=false; P in CR and not in AR
	public void test33() {
		assertEquals("cc\"cc\"c\"ccc\"BB\"c", StringUtils.replaceString("cc\"cc\"c\"ccc\"AA\"c", "AA", "BB", '\"', false));
	}

	@Test // IF=true; P not in CR
	public void test34() {
		assertEquals("AA\"cc\"c\"ccc\"cc\"c", StringUtils.replaceString("AA\"cc\"c\"ccc\"cc\"c", "AA", "BB", '\"', true));
	}

	@Test // D=NULL; IF=false; P not in CR
	public void test35() {
		assertEquals("BB\"cc\"c\"ccc\"cc\"c", StringUtils.replaceString("AA\"cc\"c\"ccc\"cc\"c", "AA", "BB", null, false));
	}

	@Test // P=NULL
	public void test36() {
		assertEquals("cc\"cc\"c\"ccc\"cc\"c", StringUtils.replaceString("cc\"cc\"c\"ccc\"cc\"c", null, "BB", '\"', true));
	}

	@Test // EC='\\'
	public void test37() {
		StringUtils.setEscape('\\');
		try {
			StringUtils.replaceString("cc\"cc\"c\"ccc\"cc\"c", "AA", "BB", '\"', true);
		} catch (RuntimeException e) {
			return;
		}
		fail();
	}
	
	@Test // D=NULL; IF=true
	public void test38() {
		StringUtils.setEscape('e'); // prevent propagation of previously set '\\'
		try {
			StringUtils.replaceString("cc\"cc\"c\"ccc\"cc\"c", "AA", "BB", null, true);
		} catch (RuntimeException e) {
			return;
		}
		fail();
	}
	
	
	
}
